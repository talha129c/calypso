ARG BASE_IMG_VER

FROM intechww-docker-local.jfrog.io/serverjre:${BASE_IMG_VER}

ARG VER

RUN useradd -d /calypso calypso

WORKDIR /calypso

ADD --chown=calypso:calypso ./target/calypso-${VER}.jar /calypso/calypso.jar

EXPOSE 1234/tcp

ENV JVM_ARGS "-D"

ADD --chown=calypso:calypso docker-entrypoint.sh /calypso/docker-entrypoint.sh
ADD --chown=calypso:calypso setup_tables.sh /calypso/setup_tables.sh

RUN chmod +x /calypso/setup_tables.sh \
             /calypso/docker-entrypoint.sh

ENTRYPOINT ["/calypso/docker-entrypoint.sh"]
USER calypso