#!/bin/sh

if [ -z "$JAVA_HOME" ] ; then
  echo "ERROR: JAVA_HOME is not set" >&2
  exit 1
fi

exec $JAVA_HOME/bin/java ${JVM_ARGS} -cp /calypso/calypso.jar com.intech.calypso.impl.CalypsoServer